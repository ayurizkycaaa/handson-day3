public class MyApp {
    public static void main(String[] args) {
        System.out.println("OBJECT MOTOR");
        Motor motor = new Motor();
        System.out.print("Suara Motor : ");
        motor.suara();
        System.out.print("Merek Motor : ");
        motor.merek();

        System.out.println();
        System.out.println("OBJECT AYAM");
        Ayam ayam = new Ayam();
        System.out.print("Suara Ayam : ");
        ayam.suara();
        System.out.print("Gerakan Ayam : ");
        ayam.tindakan();
    }
}